'use strict';
let express = require('express')
let router = express.Router();

let galleriesRouter = require('./routes/galleries')
let ImagesRouter = require('./routes/images')


// Ajout du endpoint liens à notre liste de routes
router.use('/galleries', galleriesRouter)
router.use('/images', ImagesRouter)

module.exports = router