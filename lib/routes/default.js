'use strict';

const express = require('express')
const router = express.Router()

const defaultController = require('../controlleurs/default')

router.get('/', defaultController.showDefault)
router.get('/:link', defaultController.showLink)

module.exports = router