'use strict';

let express = require('express')
let router = express.Router()

let controller = require('../controlleurs/galleries')

const passport = require('passport')

const { passportCustom } = require('../helpers/passport')
passport.use('galleryAuth', passportCustom)

const {JWT_AUTH, CHECK_PERMISSIONS} = require('../helpers/jwt-auth')


/**
 * @api         {get} /api/galleries                    Liste tous les galleries qui existent
 * @apiName     Get all galleries
 * @apiGroup    galleries
 * @apiSuccess  {Object[]}      gallery                List des galleries disponibles
 * @apiSuccess  {Date}          gallery.createdAt      Date de création de la gallerie
 * @apiSuccess  {String}        gallery.name           Nom de la gallerie
 * @apiSuccess  {String}        gallery.link           Lien de partage
 * @apiSuccess  {String}        gallery.id             Id de la gallerie
 * @apiSuccess  {Date}          gallery.date           Date de la gallerie
 * @apiSuccess  {Boolean}       gallery.readonly       Permission d'écriture
 * 
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *      [
 *          {
 *              "createdAt":"2019-08-26T21:50:08.243Z",
 *              "date":"2019-08-26T21:50:08.243Z",
 *              "_id":"5d6454a3273c32056492405f",
 *              "id":"5d6454a3273c32056492405f",
 *              "name":"Photos_2019",
 *              "link": "Tillman60com483ojzw602ih",
 *              "readonly": false
 *          }
 *      ]
 */
router.get('/', controller.show)


/**
 * @api         {get} /api/galleries/:id             Liste une gallerie spécifique avec ses images
 * @apiName     Get one gallery
 * @apiGroup    galleries
 * @apiParam    {String}        gallery.id             l'id de la gallerie
 * @apiSuccess  {Object}        gallery                La gallerie en question
 * @apiSuccess  {Date}          gallery.createdAt      Date de création de la gallerie
 * @apiSuccess  {String}        gallery.name           Nom de la gallerie
 * @apiSuccess  {Object[]}      gallery.images         Images associées à la gallerie
 * @apiSuccess  {String}        gallery.link           Lien de partage
 * @apiSuccess  {String}        gallery.id             Id de la gallerie
 * @apiSuccess  {Date}          gallery.date           Date de la gallerie
 * @apiSuccess  {Boolean}       gallery.readonly       Permission d'écriture
 * 
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *          {
 *              "createdAt":"2019-08-26T21:50:08.243Z",
 *              "date":"2019-08-26T21:50:08.243Z",
 *              "_id":"5d6454a3273c32056492405f",
 *              "id":"5d6454a3273c32056492405f",
 *              "name":"Photos_2019",
 *              "link": "Tillman60com483ojzw602ih",
 *              "readonly": false,
 *              "images": [...]
 *          }
 */
router.get('/:id', JWT_AUTH, CHECK_PERMISSIONS,controller.showOne)
router.put('/:id/auth', passport.authenticate('galleryAuth', {session: false}),controller.auth)


/**
 * @api         {post} /api/galleries/                  Création d'une gallery
 * @apiName     Création d'une gallery 
 * @apiGroup    galleries
 * @apiParam    {String}        name                    Nom de la gallerie
 * @apiParam    {String}        [password=""]           Mot de passe de la gallerie
 * @apiParam    {Date}          [date=Date.now()]       Date de la gallerie
 * @apiParam    {Boolean}       [readonly=false]        Permission d'écriture
 * @apiSuccess  {Object}        gallery                 Le gallerie en question
 * @apiSuccess  {Date}          gallery.createdAt       Date de création de la gallerie
 * @apiSuccess  {String}        gallery.name            Nom de la gallerie
 * @apiSuccess  {String}        gallery.link            lien de partage
 * @apiSuccess  {String}        gallery.id              Id de la gallerie
 * @apiSuccess  {Date}          gallery.date            Date de la gallerie
 * @apiSuccess  {Boolean}       gallery.readonly        Permission d'écriture
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 201 OK
 *          {
 *              "createdAt":"2019-08-26T21:50:08.243Z",
 *              "date":"2019-08-26T21:50:08.243Z",
 *              "_id":"5d6454a3273c32056492405f",
 *              "id":"5d6454a3273c32056492405f",
 *              "name":"Photos_2019",
 *              "link": "Tillman60com483ojzw602ih",
 *              "readonly": false,,
 *              "password": '....'
 *          }
 */
router.post('/', controller.create)
router.put('/:id', controller.modify)
router.delete('/:id', controller.delete)

module.exports = router