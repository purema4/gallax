'use strict';

let express = require('express')
const multer = require('multer')

const {JWT_AUTH} = require('../helpers/jwt-auth')
let router = express.Router()

// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         const uploadDir = path.join(__dirname, '..', '..', 'uploads')
//         cb(null, uploadDir)
//     },
//     filename: (req, file, cb) => {
//         cb(null, `${uniqid(file.filename)}${path.extname(file.originalname)}`)
//     },

// })

const storage = multer.memoryStorage()

const upload = multer({
    storage,
    limits : {
        fileSize: 5 * 1024 * 1024
    },
    fileFilter: (req,file,cb) => {

        if(file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
            cb(null, true)
        } else {
            cb(new Error("Wrong file type"), false)
        }
    }
})

let imagesController = require('../controlleurs/images')


// /api/images

router.get('/:id', JWT_AUTH, imagesController.show)
router.post('/', JWT_AUTH, upload.single('photos'), imagesController.create)
router.delete('/:id', JWT_AUTH, imagesController.delete)

module.exports = router