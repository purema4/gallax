const express = require('express')
const app = express()
const passport = require('passport')
const bodyParser = require('body-parser')
const path = require('path')


//routes
const router = require('./routes')
let defaultRouter = require('./routes/default')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(passport.initialize())

app.use(express.static(path.join(__dirname, '..','public')))
app.use('/',defaultRouter)

app.use('/images',express.static(path.join(__dirname, '..','uploads')))

//api router
app.use('/api',router)

//default view route, not api
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'))

module.exports = app