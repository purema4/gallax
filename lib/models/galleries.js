'use strict';

const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const GallerySchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Schema.Types.Date,
        default: Date.now()
    },
    password:  {
        type: Schema.Types.String,
        default: "",
    },
    date: {
        type: Schema.Types.Date,
        default: Date.now(),
    },
    link: {
        type: String,
    },
    readonly: {
        type: Schema.Types.Boolean,
        default: false,
    }

}, {
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

GallerySchema.virtual('images', {
    ref: 'Images',
    localField: '_id',
    foreignField: 'galleryID',
})

//remove all weirds caracters from the name
GallerySchema.pre('save', function(next){
    this.name = this.name.replace(/ /g, '-')
    next()
})

//Generate a unique links
GallerySchema.pre('save', function(next) {
    const uniqid = require('uniqid')
    this.link = uniqid(this.name)
    next()
})


//hash the password
GallerySchema.pre('save', function(next) {
    //encrypt the password
    if(this.password == null || this.password == "") next()
    
    bcrypt.genSalt(10)
    .then(salt => {
        bcrypt.hash(this.password, salt)
        .then(hashedPassword => {
            //console.log(hashedPassword)
            this.password = hashedPassword
            next()
        })
    })
})

GallerySchema.method('checkPassword', function(password, cb) {
    if(typeof password == 'undefined') return false
    if(this.password == null || this.password == "") return true

    bcrypt.compare(password, this.password, (err, same) => {
        if(!err) return cb(null, same)
        cb(err, false)
    })
})


module.exports = mongoose.model('Galleries', GallerySchema)