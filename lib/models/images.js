'use strict';

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ImagesSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    path: {
        type: String,
        required: true,
    },
    date: {
        type: Schema.Types.Date,
        default: Date.now()
    },
    createdAt: {
        type: Schema.Types.Date,
        default: Date.now()
    },
    galleryID:{
        type: Schema.Types.ObjectId, //String for now, but should be an array of strings
        required: true,
    }
}, {
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

module.exports = mongoose.model('Images', ImagesSchema)