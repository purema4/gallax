'use strict';

exports.showLink = (req,res) => {

    const galleryModel = require('../models/galleries')

    galleryModel.findOne({link: req.params.link})
    .then(gallery => {
        if(!gallery) {
            res.status(404).render('404')
        } else {
            res.render('link/index', {gallery})
        }
    })

}


exports.showDefault = (req,res) => {
    res.render('default/index')
}