const Images = require('../models/images')

const uniqid = require('uniqid');

const imageProcess = require('../helpers/imageProcess')



exports.show = (req, res) => {
    Images.findById(
            req.params.id
        )
        .then(image => res.json(image))
        .catch(err => res.send(err))
}

exports.create = async (req, res) => {

    const {
        galleryID
    } = req.body

    const buffer = req.file.buffer
    const [filename, extension] = req.file.originalname.split(".")
    const nameOfFile = uniqid(filename)
    const newName = `${nameOfFile}.${extension}`

    //need to resize all the images and make sure everything is wright and not oversize
    try {
        await imageProcess(buffer, req.file.originalname, newName)

        const newImage = new Images({
            name: newName,
            path: '/' + newName,
            galleryID,
        })

        const data = newImage.save()
        res.json(data)

    } catch (error) {
        res.status(400).send(error)
    }
}

exports.delete = (req, res) => {
    const {
        id
    } = req.params
    Images.findByIdAndDelete(id)
        .then(result => res.json(result))
        .catch(err => res.send(err))
}