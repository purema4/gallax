'use strict';

const Galleries = require('../models/galleries')

const {
    JWT_SIGN
} = require('../helpers/jwt-auth')


// GET
exports.show = (req, res) => {
    Galleries.find({})
        .select('-__v -password')
        .then(galleries => res.json(galleries))
        .catch(err => res.send(err))
}

exports.showOne = (req, res) => {
    const id = req.decoded.gallery_id
    Galleries.findById(id)
        .populate('images')
        .select('-__v -password')
        .then(gallery => res.json(gallery))
        .catch(err => res.send(err))
}


exports.auth = async (req, res) => {
    let token = ""
    let gallery = null
    try {
        gallery = await Galleries.findById(req.params.id)
            .select('-__V -password')
    } catch (error) {
        res.status.json({
            message: error,
            token: null
        })
        return false
    }

    const {
        id
    } = gallery

    const payload = {
        permissions: {
            galleries: [id],
            urls: [
                `GET|/api/galleries/:id`,
                'GET|/api/images/:id',
                `POST|/api/images`,
                `DELETE|/api/images/:id`
            ],
            admin: false
        }
    }

    try {
        token = await JWT_SIGN(payload, process.env.JWT_SECRET)
        res.status(200).json({
            token
        })
    } catch (error) {
        res.status(500).json({
            message: error,
            token: null
        })
    }
}


// POST
exports.create = (req, res) => {

    const name = req.body.name
    const password = req.body.password || ""
    const readonly = req.body.readonly
    const date = req.body.date || null
    const saveGallery = new Galleries({
        name,
        password,
        date,
        readonly
    })

    saveGallery.save()
        .then(gallery => res.status(201).json(gallery))
        .catch(err => res.status(500).send(err))
}

// PUT
exports.modify = (req, res) => {
    Galleries
        .findByIdAndUpdate(
            req.params.id, {
                name: req.body.name
            }, {
                new: true,
            } //return modified value instead of the old one
        )
        .then(gallery => res.json(gallery))
        .catch(err => res.send(err))
}


// DELETE
exports.delete = (req, res) => {
    Galleries.findByIdAndDelete(
            req.params.id,
        )
        .then(result => res.json(result))
        .catch(err => res.send(err))
}