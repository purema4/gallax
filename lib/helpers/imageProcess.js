const path = require('path');
const gm = require('gm').subClass({
    imageMagick: true
});
const Images = require('../models/images')


module.exports = function (buffer, originalname, newName) {

    return new Promise((resolve, reject) => {

        gm(buffer, originalname)
            .quality(75)
            .write(path.join(__dirname, '..', '..', 'uploads') + '/' + `${newName}`, (err) => {
                if (err) reject(err)
                resolve(true)
            })
    })
}