const passportCustom = require('passport-custom')
const GalleriesModel = require('../models/galleries')


let custom = new passportCustom((req,done) => {

    if(typeof req.body.password == 'undefined')
    {
        done("Must provide a password", false, {message: "Must provide a password"})
    } else {

        GalleriesModel.findById(req.params.id)
        .then(gallery => {

            if(gallery.password == "") {
                done(null,true)
            } else {
                gallery.checkPassword(req.body.password, (err, same) => {
                    if(err) done(err, false)
    
                    if(same) {
                        done(null, true)
                    } else {
                        setTimeout(function() {
                            done(null, false, {message: "Incorrect Password"})
                        }, 5 * 1000)
                    }
                })
            }
        })
        .catch(err => done(err, false))

    }
})

module.exports = {passportCustom: custom}