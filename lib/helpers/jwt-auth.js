const JWT = require('jsonwebtoken')

const imageModel = require('../models/images')

const SIGN_OPTIONS_DEFAULT = {
    expiresIn: "1h",
    issuer: "com.gallax",

}

//promessify the sign function



const JWT_SIGN = (payload = {}, secret, SIGN_OPTIONS = SIGN_OPTIONS_DEFAULT) => {
    return new Promise((resolve, reject) => {
        JWT.sign(payload, secret, SIGN_OPTIONS, (err, token) => {
            if (err) reject(err)
            resolve(token)
        })
    })
}

const JWT_VERIFY = (token, secret) => {
    return new Promise((resolve, reject) => {
        JWT.verify(token, secret, (err, decoded) => {
            if (err) reject(err)
            resolve(decoded)
        })
    })
}


const JWT_AUTH = async (req, res, next) => {

    let auth = req.headers.authorization

    if (typeof auth == 'undefined') {
        res.status(401).send("Unauthorized")
    } else {
        const token = auth.split(" ")[1]
        try {
            const decoded = await JWT_VERIFY(token, process.env.JWT_SECRET)

            // eslint-disable-next-line require-atomic-updates
            req['permissions'] = decoded.permissions
            next()
        } catch (error) {
            res.status(401).send({
                name: "JsonWebTokenError",
                error
            })
        }
    }
}


const CHECK_PERMISSIONS = async (req, res, next) => {

    const falsy = function() {
        res.status(401).json({error: "Unauthorized"})
        return false
    }

    const {
        permissions,
        originalUrl
    } = req

    //check if the path is fine
    const {urls, galleries} = permissions

    console.log(req)

    next()


    //TODO NEED TO WORK ON THAT!

    // urls.forEach(async url => {
    //     const [method, path] = url.split("|")

    //     if(req.method != method) {
    //         falsy()
    //     }

    //     //depends on the endpoint

    //     switch(path.split("/")[2]){
    //         case "galleries":
    //             if(!originalUrl.match(new RegExp(galleries[0]))) falsy()
    //             break;
    //         case "images":

    //             //check if the gallery_id has been passed if method is post
    //             if(method == "POST") {
    //                 if(!req.body.galleryID.includes(permissions.galleries[0])) {
    //                     falsy()
    //                 }
    //             } else {
    //                 imageModel.findById()
    //             }

    //             //if GET, then need to check if the id is within the gallery

    //             break;
    //         default:
    //             falsy()
    //             break;
    //     }
    // })

    // //if everything pass
    // next()
}


module.exports = {
    JWT_SIGN,
    JWT_VERIFY,
    JWT_AUTH,
    CHECK_PERMISSIONS
}