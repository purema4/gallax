'use strict';


const path = require('path')
const fs = require('fs')
const mongoose = require('mongoose')

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const app = require('./lib/app')

// mongodb
const db = process.env.NODE_ENV != 'production' ? 'gallaxDev' : 'gallax'
const host = process.env.NODE_ENV == 'production' ? 'mongo' : 'localhost'


mongoose.connect(`mongodb://${host}:27017/${db}`, {useNewUrlParser: true}, ()=> {
    console.error(`Unable to connect ${host + ":27017/" + db}, retrying`)
    //process.exit(1)
})
mongoose.set('useFindAndModify', false)




try {
    fs.mkdirSync(path.join(__dirname, 'uploads',)) 
} catch (error) {
    console.log(error)
}

app.listen(3000, () => {
    console.log('listening on port 3000')
})