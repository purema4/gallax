const mongoose = require('mongoose')

mongoose.connect(`mongodb://localhost:27017/gallaxDev`, {
    useNewUrlParser: true
})

Promise.all(
        [
            mongoose.connection.dropCollection('galleries'),
            mongoose.connection.dropCollection('images')
        ]
    ).catch(err => {
        console.error(err)
        process.exit(1)
    })
    .then(result => {
        console.log(result)
        process.exit(0)
    })