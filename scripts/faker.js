//imports

const faker = require('faker')
const mongoose = require('mongoose')


mongoose.connect(`mongodb://localhost:27017/gallaxDev`, {useNewUrlParser: true})


//clear the collection if possible
mongoose.connection.dropCollection('galleries')
    .catch(err => console.error(err))
//import models
const galleriesModel = require('../lib/models/galleries')



//generate liens
Promise.all(

    [...Array(5).keys()].map(async () => {
        const password = faker.internet.password()
        const name = faker.name.lastName()
        console.log(name, " => ", password)
        const saveGallery = new galleriesModel({
            name,
            password,
            date: faker.date.recent(),
            readonly: false
        })

        return saveGallery.save()
    })

)
.then(() => process.exit(0))
.catch(err => {console.error(err); process.exit(1)})

    
    