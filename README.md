# Gallax
Une gallerie "Self-host" qui met l'accent sur le partage rapide à partir de liens

# Documentation
Pour générer la documentation avec apidoc

```bash
$ npm run generate:doc # Génere la doc avec apiDoc
```

# RUN
## Prod
```bash
$ docker-compose up -d
```
## Dev
```bash
$ npm install
$ npm run up:dev # Part le serveur mongo
```