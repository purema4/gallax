FROM node:lts-alpine

LABEL maintainer="purema4@gmail.com"

#HEALTHCHECK --interval=5m --timeout=3s \
#  CMD curl -f http://localhost/ || exit 1

WORKDIR /usr/src/app

COPY . .

RUN apk --update add --no-cache --virtual .build-deps python python-dev make gcc g++ \
&& apk add --no-cache file imagemagick \
&& NODE_ENV=production npm install \
&& apk del .build-deps \
&& npm run generate:doc

EXPOSE 3000

CMD ["node", "index.js"]
